//
//  Segment.swift
//  iSudoku
//
//  Created by Ylli Sahiti on 2018-11-10.
//

import Foundation

class Segment {
    //MARK: Properties
    private var cells = [[Cell]]()
    private var segNr: Int
    
    //MARK: Initializers
    init() {
        for row in 0..<3 {
            self.cells.append([])
            for _ in 0..<3 {
                self.cells[row].append(Cell())
            }
        }
        segNr = -1
    }
    // passes the segNum to its cells
    func initSegment(segNr: Int) {
        self.segNr = segNr
        for row in 0..<3 {
            for col in 0..<3 {
                self.cells[row][col].initCell(segNr: self.segNr)
            }
        }
    }
    
    //MARK: Segment values
    func getCell(_ row: Int, _ col: Int) -> Cell {
        assert((0..<3).contains(row) && (0..<3).contains(col))
        return self.cells[row][col]
    }
    func getCellList() -> [[Cell]] {
        return self.cells
    }
}
