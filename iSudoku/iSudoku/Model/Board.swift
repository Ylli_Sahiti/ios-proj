//
//  Board.swift
//  iSudoku
//
//  Created by Ylli Sahiti on 2018-11-10.
//

import Foundation

enum Difficulty: Int, CaseIterable {
    case easy, medium, hard, expert
    static let mapper: [Difficulty: String] = [
        .easy: "Easy",
        .medium: "Medium",
        .hard: "Hard",
        .expert: "Expert"
    ]
    var string: String {
        return Difficulty.mapper[self]!
    }
}

class Board {
    //MARK: Properties
    private var segments = [[Segment]]()
    private var solveHistory: [Cell] = []
    
    //MARK: initializers
    init() {
        for row in 0..<3 {
            segments.append([])
            for _ in 0..<3 {
                segments[row].append(Segment())
            }
        }
        initSegments()
        initCell()
    }
    // Used to init segment numbers
    private func initSegments() {
        for row in 0..<3 {
            for col in 0..<3 {
                segments[row][col].initSegment(segNr: (row * 3) + col)
            }
        }
    }
    // Used to init cell numbers
    private func initCell() {
        for row in 0..<9 {
            for col in 0..<9 {
                let cell = self.getCell(row, col)
                cell.initCell(row: row, col: col)
            }
        }
    }
    //MARK: Serialization
    func fill(_ values: [[Int]]) {
        for row in 0..<values.count {
            for col in 0..<values[row].count {
                let value = values[row][col]
                setValue(row, col, value)
            }
        }
    }
    
    // MARK: deepCopy
    func deepCopy() -> Board {
        let copyFrom = self
        let copyTo = Board()
        for row in 0..<9 {
            for col in 0..<9 {
                let copyFromCell = copyFrom.getCell(row, col)
                let copyToCell = copyTo.getCell(row, col)
                copyToCell.deepCopy(otherCell: copyFromCell)
            }
        }
        return copyTo
    }
    func copyBoard(copyFrom: Board) -> Void {
        for row in 0..<9 {
            for col in 0..<9 {
                let copyFromCell = copyFrom.getCell(row, col)
                let copyToCell = self.getCell(row, col)
                copyToCell.deepCopy(otherCell: copyFromCell)
            }
        }
        self.solveHistory = copyFrom.solveHistory
    }
    
    //MARK: Solve history methods
    private func getHistoryList() -> [Cell] {
        return self.solveHistory
    }
    func addToHistory(cell: Cell) -> Void {
        solveHistory.append( Cell(copyFrom: cell) )
    }
    
    //MARK: board values
    func getValue(_ row: Int, _ col: Int) -> Int {
        assert((0..<9).contains(row) && (0..<9).contains(col))
        let segment = self.segments[row / 3][col / 3]
        let cell = segment.getCell(row % 3, col % 3)
        return cell.getValue()
    }
    func setValue(_ row: Int, _ col: Int, _ value: Int) -> Void {
        assert((0..<9).contains(row) && (0..<9).contains(col))
        let segment = self.segments[row / 3][col / 3]
        let cell = segment.getCell(row % 3, col % 3)
        cell.setValue(value)
    }
    
    //MARK: Board data strucutre
    func getSegment(_ row: Int, _ col: Int) -> Segment {
        assert((0..<9).contains(row) && (0..<9).contains(col))
        return self.segments[row / 3][col / 3]
    }
    func getBoardSegmentList() -> [[Segment]] {
        return self.segments
    }
    func getCell(_ row: Int, _ col: Int) -> Cell {
        assert((0..<9).contains(row) && (0..<9).contains(col))
        let segment = self.segments[row / 3][col / 3]
        return segment.getCell(row % 3, col % 3)
    }
}
