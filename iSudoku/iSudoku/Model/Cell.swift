//
//  Cell.swift
//  iSudoku
//
//  Created by Ylli Sahiti on 2018-11-14.
//

import Foundation

class Cell {
    //MARK: Properties
    private var value: Int = 0 {
        didSet {
            if self.value == 0 {
                self.resetCandidateList()
            }
            else {
                self.candidateList = []
            }
        }
    }
    private var candidateList: [Int]

    private var row: Int
    private var col: Int
    private var segNr: Int
    
    //MARK: Inititalizers
    init() {
        self.row = -1
        self.col = -1
        self.segNr = -1
        self.value = -1
        self.candidateList = []
    }
    init(copyFrom: Cell) {
        self.row = copyFrom.row
        self.col = copyFrom.col
        self.segNr = copyFrom.segNr
        self.value = copyFrom.value
        self.candidateList = copyFrom.candidateList
    }
    //MARK: Property initializers
    func initCell(segNr: Int) {
        self.segNr = segNr
    }
    func initCell(row: Int, col: Int) {
        self.row = row
        self.col = col
    }
    
    //MARK: Property methods
    func getCandidateList() -> [Int] {
        return self.candidateList
    }
    func getCandidateLength() -> Int {
        return self.candidateList.count
    }
    func resetCandidateList() -> Void {
        self.candidateList = []
        for i in 1...9 {
            self.candidateList.append(i)
        }
    }
    func setValueToFirstCL() -> Void {
        assert(!self.candidateList.isEmpty)
        self.value = self.candidateList.first!
    }
    
    func setValue(_ value: Int) -> Void {
        self.value = value
    }
    func getValue() -> Int {
        return self.value
    }
    
    
    //MARK: Positional properties
    func getRow() -> Int {
        return self.row
    }
    func getCol() -> Int {
        return self.col
    }
    func getPosition() -> (Int, Int) {
        return (self.row, self.col)
    }
    
    //MARK: Deep copy
    // cell to copy
    func deepCopy(otherCell: Cell) -> Void {
        self.value = otherCell.value
        self.candidateList = otherCell.candidateList
    }
    
    //MARK: Remove value from candidateList if existing
    @discardableResult
    func removeCandidateValue(_ value: Int) -> Bool {
        for (index, elementValue) in candidateList.enumerated() {
            if elementValue == value {
                candidateList.remove(at: index)
                return true
            }
        }
        return false
    }
}
