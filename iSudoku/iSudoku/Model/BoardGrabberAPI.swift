//
//  BoardGrabberAPI.swift
//  iSudoku
//
//  Created by Barbro Blomstedt on 2018-11-30.
//

import Foundation

//MARK: Datastructure
struct BoardStruct: Decodable {
    let text: String
    let html: String
}

//MARK: API functions, fetches new board
func getNewBoard(board: Board, group: DispatchGroup) -> Void {
    let url = URL(string: "http://motyar.info/webscrapemaster/api/?url=https://www.sudokuweb.org/&xpath=//tbody/tr/td/span")!
    
    URLSession.shared.dataTask(with: url) {
        (data, response, error) in
        guard let data = data else { return }
        
        do {
            let obj = try JSONDecoder().decode([BoardStruct].self, from: data)
            
            var qty = 0
            for cell in obj {
                if let value = Int(cell.text) {
                    let row = qty / 9
                    let col = qty % 9
                    if cell.html.range(of: "sedy") != nil {
                        board.setValue(row, col, value)
                        //values.append(cell.text)
                    }
                    else {
                        board.setValue(row, col, 0)
                        // the hidden values.. Dont use them.. It's cheating!
                    }
                    qty += 1
                }
                if qty == 81 {
                    break
                }
            }
        } catch {
            for row in 0..<9 {
                for col in 0..<9 {
                    let cell = board.getCell(row, col)
                    cell.setValue(0)
                }
            }
        }
        group.leave() // release main thread
    }.resume()
    
}
