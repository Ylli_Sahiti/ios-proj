//
//  GameRules.swift
//  iSudoku
//
//  Created by Ylli Sahiti 2018-11-14.
//

import Foundation

class GameRules {
    //MARK: Internal datastructures, used to solve the puzzle
    private enum Direction: CaseIterable {
        case row
        case col
    }
    
    //MARK: Properties
    private let board: Board! // Copy of board
    
    //MARK: Initializers
    init(board: Board) {
        self.board = board;
    }
    
    //MARK: Object initializer
    func solve() -> Bool {
        if !verifyBoard(board: self.board) {
            return false
        }
        initialCandidateElimination(board: board)
        let orgBoard = board.deepCopy()
        solveBoard(board: board)
        if !isSolved(board: board) {
            let newBoard = bruteForce(board: board)
            if newBoard != nil {
                self.board.copyBoard(copyFrom: newBoard!)
                return true
            }
            else {
                self.board.copyBoard(copyFrom: orgBoard)
                return false
            }
        }
        return isSolved(board: board)
    }
    
    //MARK: Initial solve algorithm
    private func solveBoard(board: Board) {
        
        var lastResort: Bool
        repeat { // last resort
            lastResort = false
            
            var progress: Bool
            repeat { // main loop
                progress = false
                
                while findUniqueCLValueInSegment(board: board) {
                    progress = true
                }
                
                if setCandidateValues(board: board) {
                    progress = true
                }
                
                for complexity in 2...4 {
                    if progress {
                        break
                    }
                    // run these once if first step fails, slower
                    if findNakedPairs(board: board, complexity: complexity) {
                        progress = true
                    }
                    
                    if findLockedPair(board: board, complexity: complexity) {
                        progress = true
                    }
                }
                
            } while progress
            
            if findUniqueCLInRowCol(board: board) {
                lastResort = true
            }
        } while lastResort
    }
    
    //MARK: Brute force
    // If board is not solvable by defined rules
    @discardableResult
    private func bruteForce(board: Board) -> Board? {
        if isSolved(board: board) {
            return board
        }
        if !verifyBoard(board: board) {
            return nil
        }
        
        
        let (nextRow, nextCol) = getNextCellCords(board: board)
        var tempBoard = board.deepCopy()
        var cell = tempBoard.getCell(nextRow, nextCol)
        for value in cell.getCandidateList() {
            cell.setValue(value)
            tempBoard.addToHistory(cell: cell)
            removeCLValues(board: tempBoard, cell: cell)
            solveBoard(board: tempBoard)
            let newBoard = bruteForce(board: tempBoard)
            if newBoard != nil {
                return newBoard
            }
            tempBoard = board.deepCopy()
            cell = tempBoard.getCell(nextRow, nextCol)
        }
        
        return nil
    }
    private func getNextCellCords(board: Board) -> (row: Int, col: Int) {
        // Get cords for next cell that has shortest candidateList
        var smallestSofar = 11 // needs to be lager than the length of all other candidateList
        var nextRow: Int = -1, nextCol: Int = -1
        for row in 0..<9 {
            for col in 0..<9 {
                let cell = board.getCell(row, col)
                if cell.getCandidateLength() == 0 { continue }
                if cell.getCandidateLength() < smallestSofar {
                    smallestSofar = cell.getCandidateLength()
                    (nextRow, nextCol) = cell.getPosition()
                }
            }
        }
        return (nextRow, nextCol)
    }
    
    //MARK: Board verification
    private func isSolved(board: Board) -> Bool {
        for row in 0..<9 {
            for col in 0..<9 {
                let cell = board.getCell(row, col)
                if cell.getValue() == 0 {
                    return false // found a cell that is not solved
                }
            }
        }
        return verifyBoard(board: board)
    }
    
    private func verifyBoard(board: Board) -> Bool {
        if !veryfyAll(board: board) {
            return false
        }
        if !verifySegment(board: board) {
            return false
        }
        if !verifyRow(board: board) {
            return false
        }
        if !verifyCol(board: board) {
            return false
        }
        if !verifyDoublettes(board: board) {
            return false
        }
        return true
    }
    private func veryfyAll(board: Board) -> Bool {
        // Make sure no cell has both a value of 0 and length of 0
        for row in 0..<9 {
            for col in 0..<9 {
                let cell = board.getCell(row, col)
                if cell.getValue() == cell.getCandidateLength() {
                    return false
                }
            }
        }
        return true
    }
    private func verifySegment(board: Board) -> Bool {
        // Make sure no two cells in a segment has an ambigious CL
        for row in 0..<3 {
            for col in 0..<3 {
                let segment = board.getSegment(row, col)
                
                for otherRow in 0..<3 {
                    for otherCol in 0..<3 {
                        let cell1 = segment.getCell(otherRow, otherCol)
                        if cell1.getCandidateLength() != 1 { continue }
                        for otherRow in 0..<3 {
                            for otherCol in 0..<3 {
                                let cell2 = segment.getCell(otherRow, otherCol)
                                if cell2.getCandidateLength() != 1 { continue }
                                if cell1 === cell2 { continue }
                                if (cell1.getCandidateList().first! == cell2.getCandidateList().first!) {
                                    return false
                                }
                            }
                        }
                    }
                }
            }
        }
        return true
    }
    private func verifyCol(board: Board) -> Bool {
        // Make sure no two cells in a column has an ambigious CL
        for row in 0..<9 {
            for col in 0..<9 {
                let cell1 = board.getCell(row, col)
                if cell1.getCandidateLength() != 1 { continue }
                for otherRow in 0..<9 {
                    let cell2 = board.getCell(otherRow, col)
                    if cell2.getCandidateLength() != 1 { continue }
                    if cell1 === cell2 { continue }
                    if cell1.getCandidateList().first! == cell2.getCandidateList().first! {
                        return false
                    }
                }
            }
        }
        return true
    }
    private func verifyRow(board: Board) -> Bool {
        // Make sure no two cells in a row has an ambigious CL
        for row in 0..<9 {
            for col in 0..<9 {
                let cell1 = board.getCell(row, col)
                if cell1.getCandidateLength() != 1 { continue }
                for otherCol in 0..<9 {
                    let cell2 = board.getCell(row, otherCol)
                    if cell2.getCandidateLength() != 1 { continue }
                    if cell1 === cell2 { continue }
                    if cell1.getCandidateList().first! == cell2.getCandidateList().first! {
                        return false
                    }
                }
            }
        }
        return true
    }
    private func verifyDoublettes(board: Board) -> Bool {
        var didPass = false
        if (verifyDoubletteSegment(board: board)) {
            didPass = true
        }
        if (verifyDoubletteRowCol(board: board)) {
            didPass = true
        }
        return didPass
    }
    private func verifyDoubletteSegment(board: Board) -> Bool {
        // Make sure no two cells in a segment has same values
        for segments in board.getBoardSegmentList() {
            for segment in segments {
                
                for cells in segment.getCellList() {
                    for orgCell in cells {
                        if orgCell.getValue() == 0 { continue }
                        
                        for cells in segment.getCellList() {
                            for subCell in cells {
                                if subCell === orgCell { continue }
                                if subCell.getValue() == 0 { continue }
                                if subCell.getValue() == orgCell.getValue() {
                                    return false
                                }
                            }
                        }
                    }
                }
            }
        }
        return true
    }
    
    private func verifyDoubletteRowCol(board: Board) -> Bool {
        // Make sure no two cells in a row or column has same values
        for row in 0..<9 {
            for col in 0..<9 {
                let orgCell = board.getCell(row, col)
                if orgCell.getValue() == 0 { continue }
                
                for subCol in 0..<9 {
                    let subCell = board.getCell(row, subCol)
                    if subCell.getValue() == 0 { continue }
                    if orgCell.getValue() == subCell.getValue() {
                        return false
                    }
                }
                for subRow in 0..<9 {
                    let subCell = board.getCell(subRow, col)
                    if subCell.getValue() == 0 { continue }
                    if orgCell.getValue() == subCell.getValue() {
                        return false
                    }
                }
            }
        }
        return true
    }
    
    //MARK: Initial setup of board, before solving
    private func initialCandidateElimination(board: Board) -> Void {
        // Find all cells that have a value other than 0
        // and removes CL values from other cells accordingly.
        // Runs only once per board, initially
        resetCandidateLists(board: board)
        for row in 0..<9 {
            for col in 0..<9 {
                let value = board.getValue(row, col)
                if value > 0 {
                    removeCLValues(board: board, row: row, col: col, value: value)
                }
            }
        }
    }
    private func resetCandidateLists(board: Board) -> Void {
        // Reset all cells candidate lists
        // Runs only once
        for row in 0..<9 {
            for col in 0..<9 {
                let cell = board.getCell(row, col)
                if cell.getValue() == 0 {
                    cell.resetCandidateList()
                }
            }
        }
    }
    
    //MARK: Rule 1
    private func setCandidateValues(board: Board) -> Bool {
        // Set all cell values with CL length of 1 to that CL value
        var valueChanged = false
        for row in 0..<9 {
            for col in 0..<9 {
                let cell = board.getCell(row, col)
                if cell.getCandidateLength() == 1 {
                    cell.setValueToFirstCL()
                    board.addToHistory(cell: cell)
                    removeCLValues(board: board, row: row, col: col, value: cell.getValue())
                    valueChanged = true
                }
            }
        }
        return valueChanged
    }
    private func findUniqueCLInRowCol(board: Board) -> Bool {
        // Find a cell that has a unique value in its CL
        // compared to others in same row AND col.
        // If found, assign that cell that value
        var changedValue = false
        for row in 0..<9 {
            nextCell: for col in 0..<9 {
                let cell = board.getCell(row, col)
                for value in cell.getCandidateList() {
                    for dir in Direction.allCases {
                        for subDir in 0..<9 {
                            var subCell: Cell
                            switch dir {
                            case .row:
                                subCell = board.getCell(row, subDir)
                            case .col:
                                subCell = board.getCell(subDir, col)
                            }
                            if subCell.getCandidateList().contains(value) { continue nextCell }
                        }
                        // Try to find this value in another cell in same col
                    }
                    cell.setValue(value)
                    board.addToHistory(cell: cell)
                    changedValue = true
                    let (cellRow, cellCol) = cell.getPosition()
                    removeCLValuesInSegment(board.getSegment(cellRow, cellCol), value)
                }
            }
        }
        return changedValue
    }
    private func findUniqueCLValueInSegment(board: Board) -> Bool {
        // Find a cell that has a unique value in CL within its segment
        var didSetValue = false
        for segments in board.getBoardSegmentList() {
            for segment in segments {
                for cells in segment.getCellList() {
                    for orgCell in cells {
                        
                        nextValue: for value in orgCell.getCandidateList() {
                            for subCells in segment.getCellList() {
                                for subCell in subCells {
                                    // find a cell within segment
                                    // that dont have "value" in its CL
                                    if orgCell === subCell { continue }
                                    if subCell.getCandidateList().contains(value) {
                                        // this value exists in cell2 within segment
                                        // get next value and try again
                                        continue nextValue
                                    }
                                }
                            }
                            orgCell.setValue(value)
                            board.addToHistory(cell: orgCell)
                            removeCLValues(board: board, cell: orgCell)
                            didSetValue = true
                            break
                        }
                    }
                }
            }
        }
        return didSetValue
    }
    
    //MARK: Rule 2
    private func findNakedPairs(board: Board, complexity: Int) -> Bool {
        var foundPair = false
        
        for row in 0..<9 {
            for col in 0..<9 {
                var cells: [Cell] = []
                cells = [board.getCell(row, col)]
                // look through an entire column and row for this cell
                if !(2...complexity).contains(cells.first!.getCandidateLength()) { continue }
                // first cell needs to be 2, 3 or 4 in length
                
                for dir in Direction.allCases {
                    var nextCellDir = 0
                    cells = [cells.first!]
                    while nextCellDir <= (9 - (cells.first!.getCandidateLength() - cells.count)) {
                        var subCell: Cell
                        var fixedPos: Int
                        switch dir {
                        case .row:
                            subCell = board.getCell(row, nextCellDir)
                            fixedPos = row
                        case .col:
                            subCell = board.getCell(nextCellDir, col)
                            fixedPos = col
                        }
                        nextCellDir += 1
                        if !findVerifySubCell(orgCell: cells.first!, subCell: subCell) { continue }
                        cells.append(subCell)
                        
                        if cells.count == cells.first!.getCandidateLength() {
                            if findCheckAllObjects(cells: cells) {
                                if removeNakedInRowCol(board: board, fixedPos: fixedPos, cells: cells, direction: dir) {
                                    foundPair = true
                                }
                            }
                            break
                        }
                    }
                }
            }
        }
        return foundPair
    }
    private func removeNakedInRowCol(board: Board, fixedPos: Int, cells: [Cell], direction: Direction) -> Bool {
        assert(cells.count > 0)
        for i in 1..<cells.count {
            assert((2...cells.count).contains(cells[i].getCandidateLength()))
        }
        
        var removedCLValue = false
        
        startLoop: for nextPos in 0..<9 {
            
            var cell: Cell
            switch direction {
            case .row:
                cell = board.getCell(fixedPos, nextPos)
            case .col:
                cell = board.getCell(nextPos, fixedPos)
            }
            
            if cell.getCandidateLength() == 0 { continue } // nothing to remove
            for subCell in cells {
                if cell === subCell { continue startLoop } // this is one of the original cells
            }
            
            for value in cells.first!.getCandidateList() { // original cell
                if cell.removeCandidateValue(value) {
                    removedCLValue = true
                }
            }
        }
        
        return removedCLValue
    }
    
    
    //MARK: Rule 3
    private func findLockedPair(board: Board, complexity: Int) -> Bool {
        var foundPair = false
        
        for segments in board.getBoardSegmentList() {
            for segment in segments {
                
                for cells in segment.getCellList() {
                    newCell: for orgCell in cells {
                        var cells: [Cell] = []
                        cells = [orgCell]
                        if !(2...complexity).contains(cells.first!.getCandidateLength()) { continue }
                        
                        var subCellsTested = 0
                        for subCells in segment.getCellList() {
                            for subCell in subCells {
                                subCellsTested += 1
                                if !findVerifySubCell(orgCell: cells.first!, subCell: subCell) { continue }
                                cells.append(subCell)
                                
                                if cells.count == cells.first!.getCandidateLength() {
                                    if findCheckAllObjects(cells: cells) {
                                        if removeLockedInSegment(segment: segment, cells: cells) {
                                            foundPair = true
                                        }
                                    }
                                    continue newCell
                                }
                                if subCellsTested <= (9 - (cells.first!.getCandidateLength() - cells.count)) {
                                    continue newCell
                                }
                            }
                        }
                        
                    }
                }
            }
        }
        return foundPair
    }
    private func removeLockedInSegment(segment: Segment, cells: [Cell]) -> Bool {
        assert(cells.count > 0)
        for i in 1..<cells.count {
            assert((2...cells.count).contains(cells[i].getCandidateLength()))
        }
        
        var removedCLValue = false
        
        // removes candidatevalues from segment
        for cells in segment.getCellList() {
            newCell: for cell in cells {
                if cell.getCandidateLength() == 0 { continue newCell }
                for pairCell in cells {
                    if cell === pairCell { continue newCell }
                }
                
                for value in cells.first!.getCandidateList() { // original cell
                    if cell.removeCandidateValue(value) {
                        removedCLValue = true
                    }
                }
            }
        }
        
        // if they are potentially in line by row or col
        if (2...3).contains(cells.count) {
            var currCell: Cell
            // check row first
            currCell = cells.first!
            for i in 1..<cells.count {
                if currCell.getRow() == cells[i].getRow() {
                    currCell = cells[i]
                }
                else {
                    break
                }
            }
            if currCell === cells.last {
                let fixedPos = cells.first!.getRow()
                let dir = Direction.row
                if (removeNakedInRowCol(board: board, fixedPos: fixedPos, cells: cells, direction: dir)) {
                    removedCLValue = true
                }
            }
            
            // check col then
            currCell = cells.first!
            for i in 1..<cells.count {
                if currCell.getCol() == cells[i].getCol() {
                    currCell = cells[i]
                }
                else {
                    break
                }
            }
            if currCell === cells.last {
                let fixedPos = cells.first!.getCol()
                let dir = Direction.col
                if (removeNakedInRowCol(board: board, fixedPos: fixedPos, cells: cells, direction: dir)) {
                    removedCLValue = true
                }
            }
        }
        
        return removedCLValue
    }
    
    //MARK: findNaked and findLocked object verifiers
    private func findCheckAllObjects(cells: [Cell]) -> Bool {
        assert(cells.count == cells.first!.getCandidateLength())
        for i in 1..<cells.count {
            assert((2...cells.first!.getCandidateLength()).contains(cells[i].getCandidateLength()))
        }
        
        var list: [Int] = []
        
        for i in 1..<cells.first!.getCandidateLength() {
            for value in cells[i].getCandidateList() {
                if !list.contains(value) {
                    list.append(value)
                }
            }
        }
        list.sort()
        
        return cells.first!.getCandidateList() == list
    }
    
    private func findVerifySubCell(orgCell: Cell, subCell: Cell) -> Bool {
        if orgCell === subCell {
            return false
        }
        if !(2...orgCell.getCandidateLength()).contains(subCell.getCandidateLength()) {
            // if wrong length
            return false
        }
        
        for value in subCell.getCandidateList() {
            if !orgCell.getCandidateList().contains(value) {
                // value in other cells does not exist in main cell
                return false
            }
        }
        return true
    }
    
    //MARK: Cleaners
    private func removeCLValues(board: Board, cell: Cell) -> Void {
        //
        let (row, col) = cell.getPosition()
        removeCLValues(board: board, row: row, col: col, value: cell.getValue())
    }
    private func removeCLValues(board: Board, row: Int, col: Int, value: Int) -> Void {
        // Remove value "value" from the corresponding segment, row and cols
        removeCLValuesInSegment(board.getSegment(row, col), value)
        
        removeCLValuesInRow(board: board, row: row, value: value)
        removeCLValuesInCol(board: board, col: col, value: value)
    }
    private func removeCLValuesInSegment(_ segment: Segment, _ value: Int) {
        for row in 0..<3 {
            for col in 0..<3 {
                segment.getCell(row, col).removeCandidateValue(value)
            }
        }
    }
    private func removeCLValuesInRow(board: Board, row: Int, value: Int) {
        for col in 0..<9 {
            board.getCell(row, col).removeCandidateValue(value)
        }
    }
    private func removeCLValuesInCol(board: Board, col: Int, value: Int) {
        for row in 0..<9 {
            board.getCell(row, col).removeCandidateValue(value)
        }
    }
    
}
