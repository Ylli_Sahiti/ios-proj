//
//  GameController.swift
//  iSudoku
//
//  Created by Ylli Sahiti on 2018-11-10.
//

import Foundation

class GameController {
    //MARK: Properties
    private let board: Board
    private let rules: GameRules
    
    var defaults = UserDefaults.standard
    
    //MARK: Initializers
    init() {
        self.board = Board()
        self.rules = GameRules(board: self.board)
    }
    
    //MARK: Load and save
    func loadBoard() {
        if let savedBoard = defaults.object(forKey: "savedBoard") as? [[Int]] {
           self.board.fill(savedBoard)
        }
    }
    func saveBoard() {
        var savedBoard = [[Int]]()
        for row in 0..<9 {
            var rowArr = [Int]()
            for col in 0..<9 {
                let value = self.board.getValue(row,col)
                rowArr.append(value)
            }
            savedBoard.append(rowArr)
        }
        defaults.set(savedBoard, forKey: "savedBoard")
    }
    
    func getRandomBoard() {
            let group = DispatchGroup()
            group.enter()
            getNewBoard(board: self.board, group: group)
            group.wait() // wait to get released by http request thread
    }
 
    //MARK: Board manipulation methods
    func solve() -> Bool {
        return self.rules.solve()
    }
    func getBoard() -> Board {
        return self.board
    }
    func setValue(_ row: Int, _ col: Int, _ value: Int!) -> Bool {
        board.setValue(row, col, value)
        return true
    }
}
