//
//  ViewController.swift
//  iSudoku
//
//  Created by Ylli Sahiti on 2018-11-06.
//

import UIKit

class UIWelcome: UIViewController {
    
    private var gameControl = GameController()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func btnCreate(_ sender: Any) {
        // Create button
    }
    
    @IBAction func btnContinue(_ sender: Any) {
        // Continue button
    }
    @IBAction func takePhotoTapped(_ sender: Any) {
        CameraHandler.shared.showActionSheet(vc: self)
        CameraHandler.shared.imagePickedBlock = { (image) in
            self.photoContainer.image = image
        }
    }
    
   
    
    
    @IBOutlet weak var photoContainer: UIImageView!
    
 
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "ContinueGame"){
            let game = segue.destination as! UIGame
            game.loadGame = true
        }
        if(segue.identifier == "NewGame"){
            let game = segue.destination as! UIGame
            game.loadGame = false
        }
    }
    
}

