//
//  UISelectGame.swift
//  iSudoku
//
//  Created by Anaïs Schönborg on 2018-11-17.
//

import Foundation
import UIKit

class UISelectGame : UIViewController {
    
    // Needed because its a UI class
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        for diff in Difficulty.allCases {
            let button = UIButton()
            button.setTitle(diff.string, for: [])
            button.tag = diff.rawValue
            button.setTitleColor(UIColor.black, for: [])
            button.titleLabel?.font = UIFont(name: "Noteworthy-bold", size: 35)
            button.titleLabel?.adjustsFontSizeToFitWidth = true
            button.addTarget(self,
                             action: #selector(onDifficultySelected),
                             for: .touchUpInside
            )
            buttonsContainer.addArrangedSubview(button)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "StartNewGame"){
            let game = segue.destination as! UIGame
            guard let button = sender as? UIButton else { return }
            game.loadGame = false
            let diffValue = Difficulty(rawValue: button.tag)
            game.difficulty = diffValue!
        }
    }
    
    @IBAction func onDifficultySelected(sender:UIGridCell){
        performSegue(withIdentifier:"StartNewGame", sender:sender);
    }
    
    @IBOutlet weak var buttonsContainer: UIStackView!
}
