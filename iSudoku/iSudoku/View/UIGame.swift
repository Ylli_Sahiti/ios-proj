//
//  GameView.swift
//  iSudoku
//
//  Created by Ylli Sahiti on 2018-11-10.
//

import Foundation
import UIKit
import Spring

class UIGame : UIViewController {
    var loadGame = true
    var difficulty: Difficulty = .medium
    private var controller = GameController()
    
    //MARK: View Making methods
    func createGrid(){
        for row in 0...8{
            let rowView = UIStackView()
            rowView.axis = .horizontal
            rowView.distribution = .fillEqually
            for col in 0...8{
                let cell = UIGridCell(row: row, col: col)
                cell.setTitle("", for: [])
                cell.addTarget(self,
                               action: #selector(onCellClicked),
                               for: .touchUpInside
                )
                rowView.addArrangedSubview(cell)
            }
            gridContainer.addArrangedSubview(rowView)
            gridContainer.sizeToFit()
        }
    }

    func resetGrid(){
        for subview in gridContainer.subviews {
            for case let cell as UIGridCell in subview.subviews {
                if cell.currentTitle == "" {
                    continue
                }
                cell.setTitle("", for: .normal)
            }
        }
    }
    
    
    func fillGrid(board: Board){
        var delay: CGFloat = 0.0
        for subview in gridContainer.subviews
        {
            for case let cell as UIGridCell in subview.subviews
            {
                if cell.currentTitle != "" {
                    continue
                }
                let title = toString(board.getValue(cell.row,cell.col))
                if title == "" {
                    continue
                }
                cell.fadeInTitle(title: title, delay: delay)
                
                delay += 0.03
            }
        }
    }
    
    func fillCell(row: Int, col: Int, value: Int) {
        for subview in gridContainer.subviews
        {
            for case let cell as UIGridCell in subview.subviews
            {
                if row == cell.row && col == cell.col {
                    cell.setTitle(toString(value), for: .normal)
                }
            }
        }
    }
    //MARK: Functions
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "NumPicker"){
            guard let cell = sender as? UIGridCell else { return }
            let row = cell.row
            let col = cell.col
            let numPicker = segue.destination as! UINumPicker
            numPicker.callback = { (value) in
                let validMove = self.controller.setValue(row, col, value)
                if(validMove) {
                    self.fillCell(row: row, col: col, value: value)
                }
            }
            
        }
    }
    
    @IBAction func onCellClicked(sender:UIGridCell){
        performSegue(withIdentifier:"NumPicker", sender:sender);
    }
    
    // Needed because its a UI class
    override func viewDidLoad() {
        super.viewDidLoad()
        self.edgesForExtendedLayout = []
        // Do any additional setup after loading the view, typically from a nib.
        createGrid()
        gridImage.animation = "flipX"
        gridImage.repeatCount = loadGame ? 1 : 10
        gridImage.duration = loadGame ? 0.8 : 0.2
        gridImage.animate()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
       
        if loadGame {
            controller.loadBoard()
        } else {
            controller.getRandomBoard()
        }
        fillGrid(board: controller.getBoard())
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        controller.saveBoard()
    }
    
    @IBAction func printTheBoardj(_ sender: Any) {
        if controller.solve() {
            print("Solved!")
        }
        else {
            print("Not solvable")
        }
        self.fillGrid(board: self.controller.getBoard())
    }
    
    func toString(_ value: Int) -> String {
            return value > 0 ? String(value) : ""
    }
    
    //MARK: - Constants, Outlets and Properties
    
    @IBOutlet weak var gridContainer: UIStackView!
    @IBOutlet weak var gridImage: SpringImageView!
}


