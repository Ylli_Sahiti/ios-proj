//
//  UINumPicker.swift
//  
//
//  Created by Anaïs Schönborg on 2018-11-12.
//

import Foundation
import UIKit

class UINumPicker : UIViewController {
    
    var callback: ((_ value: Int) -> ())?
    
    //MARK: View Making methods
    func createButtons(count:Int){
        var value: Int = 1
        for _ in 0...2{
            let rows = UIStackView()
            rows.axis = .horizontal
            rows.distribution = .fillEqually
            rows.spacing = 5.0
            for _ in 0...2{
                let button = createButton(value: value)
                rows.addArrangedSubview(button)
                value += 1
            }
            ButtonContainer.addArrangedSubview(rows)
            ButtonContainer.spacing = 5.0
        }
        
    }
    func createButton(value: Int) -> UIButton {
        let button = UIButton()
        button.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.7)
        button.layer.cornerRadius = 5.0
        let title = String(value)
        button.setTitle(title, for: [])
        button.tag = value
        button.setTitleColor(UIColor.white, for: [])
        button.addTarget(self,
                       action: #selector(onButtonClicked),
                       for: .touchUpInside
        )
        return button
    }
    
   
    // Needed because its a UI class
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        createButtons(count: 9)
    }
    //MARK: - Actions and Selectors
    @IBAction func onButtonClicked(sender:UIButton){
        let value = sender.tag
        Dismiss(sender)
        callback?(value)
    }
   
    @IBAction func Dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    //MARK: - Constants, Outlets and Properties
    
    @IBOutlet weak var ButtonContainer: UIStackView!
    
    
    
}
