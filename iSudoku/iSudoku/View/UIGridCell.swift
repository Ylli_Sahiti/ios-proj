//
//  UIGridCell.swift
//  
//
//  Created by Anaïs Schönborg on 2018-11-12.
//

import Spring

class UIGridCell: SpringButton {
    
    var row: Int
    var col: Int
    
    required init(row: Int, col: Int) {
        // set myValue before super.init is called
        self.row = row
        self.col = col
        
        super.init(frame: .zero)
        
        self.setTitleColor(UIColor.black, for: [])
        self.titleLabel?.font = UIFont(name: "Noteworthy-bold", size: 25)
        self.titleLabel?.adjustsFontSizeToFitWidth = true
    
        // set other operations after super.init, if required
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func fadeInTitle(title: String, delay: CGFloat) {
        animation = "fadeIn"
        self.delay = delay
        duration = 0.3
        animate()
        setTitle(title, for: .normal)
    }
}
